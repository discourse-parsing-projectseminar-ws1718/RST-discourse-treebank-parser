class Tree:
    root_node = None
    text_list = []
    
    class Node:
        data = -1
        parent = None
        left = None
        right = None
        leftNucleus = False
        rightNucleus = False
        
        def print(self, indent=""):
            print(f"{indent}data: {self.data}")
            print(f"{indent}left({self.leftNucleus}):")
            if self.left != None:
                self.left.print(indent + "  ")
            print(f"{indent}right({self.rightNucleus}):")
            if self.right != None:
                self.right.print(indent + "  ")

        def add_node(self, node, nucleus):
            if self.left == None:
                self.left = node
                self.leftNucleus = nucleus
            elif self.right == None:
                self.right = node
                self.rightNucleus = nucleus
            else:
                parent = self
                while parent.right.right != None:
                    parent = parent.right
                inner = Tree.Node()
                inner.data = parent.data
                inner.parent = parent
                inner.left = parent.right
                inner.leftNucleus = parent.rightNucleus
                inner.right = node
                inner.rightNucleus = nucleus
                parent.right = inner
                parent.rightNucleus = True
        
    def print(self):
        print(self.text_list)
        self.root_node.print()
