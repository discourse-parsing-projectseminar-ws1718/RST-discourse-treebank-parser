#!/usr/bin/env python3
# TODO: make sure python 3 is used

import os
import sys
from discourse_tree import *
from dependency_tree import *
from enum import Enum, auto
        
class TokenType(Enum):
    PAROPEN = auto()
    PARCLOSE = auto()
    
    ROOT = auto()
    NUCLEUS = auto()
    SATELLITE = auto()
    LEAF = auto()
    SPAN = auto()
    REL2PAR = auto()
    TEXT = auto()
    
    RELATION = auto()
    NUMBER = auto()
    CONTENT = auto()
    
keyword_to_tokentype = {
    "Root": TokenType.ROOT,
    "Nucleus": TokenType.NUCLEUS,
    "Satellite": TokenType.SATELLITE,
    "leaf": TokenType.LEAF,
    "span": TokenType.SPAN,
    "rel2par": TokenType.REL2PAR,
    "text": TokenType.TEXT
}

categories = [
    "Attribution",
    "Background",
    "Cause",
    "Comparison",
    "Condition",
    "Contrast",
    "Elaboration",
    "Enablement",
    "Evaluation",
    "Explanation",
    "Joint",
    "Manner-Means",
    "Topic-Comment",
    "Summary",
    "Temporal",
    "Topic-Change",
    "Other"
]

relation_to_category = {
    "attribution": 0,
    "attribution-e": 0,
    "attribution-n": 0,
    "background": 1,
    "background-e": 1,
    "circumstance": 1,
    "circumstance-e": 1,
    "cause": 2,
    "result": 2,
    "result-e": 2,
    "Consequence": 2,
    "consequence-n": 2,
    "consequence-n-e": 2,
    "consequence-s": 2,
    "consequence-s-e": 2,
    "Comparison": 3,
    "comparison": 3,
    "comparison-e": 3,
    "preference": 3,
    "preference-e": 3,
    "Analogy": 3,
    "analogy": 3,
    "analogy-e": 3,
    "Proportion": 3,
    "condition": 4,
    "condition-e": 4,
    "hypothetical": 4,
    "contingency": 4,
    "Otherwise": 4,
    "otherwise": 4,
    "Contrast": 5,
    "concession": 5,
    "concession-e": 5,
    "antithesis": 5,
    "antithesis-e": 5,
    "elaboration-additional": 6,
    "elaboration-additional-e": 6,
    "elaboration-general-specific": 6,
    "elaboration-general-specific-e": 6,
    "elaboration-part-whole": 6,
    "elaboration-part-whole-e": 6,
    "elaboration-process-step": 6,
    "elaboration-process-step-e": 6,
    "elaboration-object-attribute": 6,
    "elaboration-object-attribute-e": 6,
    "elaboration-set-member": 6,
    "elaboration-set-member-e": 6,
    "example": 6,
    "example-e": 6,
    "definition": 6,
    "definition-e": 6,
    "purpose": 7,
    "purpose-e": 7,
    "enablement": 7,
    "enablement-e": 7,
    "Evaluation": 8,
    "evaluation-n": 8,
    "evaluation-s": 8,
    "evaluation-s-e": 8,
    "Interpretation": 8,
    "interpretation-n": 8,
    "interpretation-s": 8,
    "interpretation-s-e": 8,
    "conclusion": 8,
    "comment": 8,
    "comment-e": 8,
    "evidence": 9,
    "evidence-e": 9,
    "explanation-argumentative": 9,
    "explanation-argumentative-e": 9,
    "Reason": 9,
    "reason": 9,
    "reason-e": 9,
    "List": 10,
    "Disjunction": 10,
    "manner": 11,
    "manner-e": 11,
    "means": 11,
    "means-e": 11,
    "Problem-Solution": 12,
    "problem-solution-n": 12,
    "problem-solution-s": 12,
    "Question-Answer": 12,
    "question-answer-n": 12,
    "question-answer-s": 12,
    "statement-response-n": 12,
    "statement-response-s": 12,
    "Statement-Response": 12,
    "Topic-Comment": 12,
    "Comment-Topic": 12,
    "rhetorical-question": 12,
    "summary-n": 13,
    "summary-s": 13,
    "restatement": 13,
    "restatement-e": 13,
    "temporal-before": 14,
    "temporal-before-e": 14,
    "temporal-after": 14,
    "temporal-after-e": 14,
    "temporal-same-time": 14,
    "Temporal-Same-Time": 14,
    "temporal-same-time-e": 14,
    "Sequence": 14,
    "Inverted-Sequence": 14,
    "topic-shift": 15,
    "Topic-Shift": 15,
    "topic-drift": 15,
    "Topic-Drift": 15,
    "Cause-Result": 16,
    "Same-Unit": 16,
    "TextualOrganization": 16
}

class Token:
    ttype = -1
    tvalue = -1
    
    def __init__(self, t, v = -1):
        self.ttype = t
        self.tvalue = v

def lexer(content):
    tokenList = []
    i = 0
    while i < len(content):
        if content[i] == "(":
            tokenList.append(Token(TokenType.PAROPEN))
        elif content[i] == ")":
            tokenList.append(Token(TokenType.PARCLOSE))
        elif content[i] == " ":
            pass
        elif content[i] == "\n":
            pass
        elif content[i] == "\r":
            pass
        elif content[i] == "\t":
            pass
        elif content[i:i+2] == "_!":
            i += 2
            end = content.find("_!", i)
            if end == -1:
                print("Invalid file: leaf text missing!")
                sys.exit(1)
            tokenList.append(Token(TokenType.CONTENT, content[i:end]))
            i = end + 2
            continue
        elif content[i].isdigit():
            j = i+1
            while j < len(content) and content[j].isdigit():
                j += 1
            tokenList.append(Token(TokenType.NUMBER, int(content[i:j])))
            i = j
            continue
        elif content[i].isalpha():
            j = i+1
            while j < len(content) and (content[j].isalnum() or content[j] == "-"):
                j += 1
            if content[i:j] in keyword_to_tokentype:
                tokenList.append(Token(keyword_to_tokentype[content[i:j]]))
            else:
                tokenList.append(Token(TokenType.RELATION, content[i:j]))
            i = j
            continue
        else:
            print("Invalid file: contains invalid symbol!")
            sys.exit(1)

        i += 1
        
    return tokenList

class unexpectedTokenException(Exception):
    def __init__(self, tokentype, expected):
        print(f"Found unexpected {tokentype} while expecting {expected}!")

def expect(token, *args):
    if token.ttype in args:
        return token.ttype
    else:
        raise unexpectedTokenException(token.ttype, args)

def parser(tokenList):
    contentList = []
    expect(tokenList[0], TokenType.PAROPEN)
    expect(tokenList[1], TokenType.ROOT)
    expect(tokenList[2], TokenType.PAROPEN)
    expect(tokenList[3], TokenType.SPAN)
    expect(tokenList[4], TokenType.NUMBER)
    expect(tokenList[5], TokenType.NUMBER)
    expect(tokenList[6], TokenType.PARCLOSE)
    root = Tree.Node()
    stack = []
    stack.append((root, (1, tokenList[5].tvalue)))
    c = 7

    while len(stack) > 0:
        begin = ((stack[-1])[1])[0]
        end = ((stack[-1])[1])[1]

        if begin > end:
            stack.pop()

            if len(stack) <= 0:
                break

            top = stack[-1]
            stack[-1] = (top[0], (begin, (top[1])[1]))
            
            expect(tokenList[c], TokenType.PARCLOSE)
            c += 1
            continue
        else: #begin > end
            node = Tree.Node()
            node.parent = (stack[-1])[0]
            expect(tokenList[c], TokenType.PAROPEN)

        if TokenType.NUCLEUS == expect(tokenList[c+1], TokenType.NUCLEUS, TokenType.SATELLITE):
            node.parent.add_node(node, True)
            expect(tokenList[c+2], TokenType.PAROPEN)

            if TokenType.LEAF == expect(tokenList[c+3], TokenType.LEAF, TokenType.SPAN):
                expect(tokenList[c+4], TokenType.NUMBER)
                expect(tokenList[c+5], TokenType.PARCLOSE)
                expect(tokenList[c+6], TokenType.PAROPEN)
                expect(tokenList[c+7], TokenType.REL2PAR)
                token = expect(tokenList[c+8], TokenType.SPAN, TokenType.RELATION)
                expect(tokenList[c+9], TokenType.PARCLOSE)
                expect(tokenList[c+10], TokenType.PAROPEN)
                expect(tokenList[c+11], TokenType.TEXT)
                expect(tokenList[c+12], TokenType.CONTENT)
                contentList.append(tokenList[c+12].tvalue)
                node.data = len(contentList) - 1
                expect(tokenList[c+13], TokenType.PARCLOSE)
                expect(tokenList[c+14], TokenType.PARCLOSE)

                top = stack[-1]
                stack[-1] = (top[0], ((top[1])[0] + 1, (top[1])[1]))

                if token != TokenType.SPAN:
                    node.parent.data = relation_to_category[tokenList[c+8].tvalue]

                c += 15

            else: #TokenType.LEAF == expect(tokenList[c+3], TokenType.LEAF, TokenType.SPAN)
                expect(tokenList[c+4], TokenType.NUMBER)
                begin = tokenList[c+4].tvalue
                expect(tokenList[c+5], TokenType.NUMBER)
                end = tokenList[c+5].tvalue
                expect(tokenList[c+6], TokenType.PARCLOSE)
                expect(tokenList[c+7], TokenType.PAROPEN)
                expect(tokenList[c+8], TokenType.REL2PAR)
                token = expect(tokenList[c+9], TokenType.SPAN, TokenType.RELATION)
                expect(tokenList[c+10], TokenType.PARCLOSE)

                stack.append((node, (begin, end)))

                if token != TokenType.SPAN:
                    node.parent.data = relation_to_category[tokenList[c+9].tvalue]

                c += 11

        else: #TokenType.NUCLEUS == expect(tokenList[c+1], TokenType.NUCLEUS, TokenType.SATELLITE)
            node.parent.add_node(node, False)
            expect(tokenList[c+2], TokenType.PAROPEN)

            if TokenType.LEAF == expect(tokenList[c+3], TokenType.LEAF, TokenType.SPAN):
                expect(tokenList[c+4], TokenType.NUMBER)
                expect(tokenList[c+5], TokenType.PARCLOSE)
                expect(tokenList[c+6], TokenType.PAROPEN)
                expect(tokenList[c+7], TokenType.REL2PAR)
                expect(tokenList[c+8], TokenType.RELATION)
                node.parent.data = relation_to_category[tokenList[c+8].tvalue]
                expect(tokenList[c+9], TokenType.PARCLOSE)
                expect(tokenList[c+10], TokenType.PAROPEN)
                expect(tokenList[c+11], TokenType.TEXT)
                expect(tokenList[c+12], TokenType.CONTENT)
                contentList.append(tokenList[c+12].tvalue)
                node.data = len(contentList) - 1
                expect(tokenList[c+13], TokenType.PARCLOSE)
                expect(tokenList[c+14], TokenType.PARCLOSE)

                top = stack[-1]
                stack[-1] = (top[0], ((top[1])[0] + 1, (top[1])[1]))

                c += 15
                
            else: #TokenType.LEAF == expect(tokenList[c+3], TokenType.LEAF, TokenType.SPAN)
                expect(tokenList[c+4], TokenType.NUMBER)
                begin = tokenList[c+4].tvalue
                expect(tokenList[c+5], TokenType.NUMBER)
                end = tokenList[c+5].tvalue
                expect(tokenList[c+6], TokenType.PARCLOSE)
                expect(tokenList[c+7], TokenType.PAROPEN)
                expect(tokenList[c+8], TokenType.REL2PAR)
                expect(tokenList[c+9], TokenType.RELATION)
                node.parent.data = relation_to_category[tokenList[c+9].tvalue]
                expect(tokenList[c+10], TokenType.PARCLOSE)

                stack.append((node, (begin, end)))

                c += 11

    expect(tokenList[c], TokenType.PARCLOSE)
    # root.print()
    tree = Tree()
    tree.root_node = root
    tree.text_list = contentList
    return tree

def main():
    if len(sys.argv) != 3:
        print("Must supply exactly 2 directories!")
        sys.exit(1)
    in_arg = os.path.join(sys.argv[1], '')
    out_arg = os.path.join(sys.argv[2], '')
    if not os.path.isdir(in_arg):
        print(f"Not a directory! {in_arg}")
        sys.exit(1)
    if not os.path.isdir(out_arg):
        print(f"Not a directory! {out_arg}")
        sys.exit(1)
    entries = map(lambda x: in_arg + x, os.listdir(in_arg))
    for elem in entries:
        if not os.path.isfile(elem):
            continue
        # TODO: process file
        # print(elem)
        file = open(elem, 'r')
        content = file.read()
        # print(content)
        resultTokenList = lexer(content)
        # for token in resultTokenList:
        #    print(f"{token.ttype}, {token.tvalue}")
        tree = parser(resultTokenList)
        transform(tree)

if __name__ == "__main__":
    main()
