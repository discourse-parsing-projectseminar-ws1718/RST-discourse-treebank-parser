#!/bin/bash -eu

find $1 -name '*.dis' |
	xargs -I {} cat {} |
	grep rel2par |
	sed "s/^.*(rel2par \([[:alpha:],-]*\)).*$/\1/" |
	sort |
	uniq -c
