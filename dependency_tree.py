class Dep_Tree:
	root_node = None
	text_list = []

	class Dep_Node:
		data = -1
		parent = None
		firstChild = None
		secondChild = None
		nucleus = False

def transform(diTree):
	depTree = Dep_Tree()
	depTree.text_list = diTree.text_list
	depTree.root_node = Dep_Tree.Dep_Node()
	
	tnode = diTree.root_node
	dtnode = depTree.root_node
	
	stack = []
	while (tnode.leftNucleus or tnode.rightNucleus):
		stack.append(0)
		if tnode.leftNucleus:
			tnode = tnode.left
		else:
			tnode = tnode.right
	
	isNucleus = True
	while (True):
		newnode = Dep_Tree.Dep_Node()
		newnode.data = tnode.data
		newnode.parent = dtnode
		newnode.nucleus = isNucleus
		if dtnode.firstChild == None:
			dtnode.firstChild = newnode
		else:
			dtnode.secondChild = newnode
		stack[-1] += 1
		dtnode = newnode
		
		isleft = tnode.parent.left == tnode
		tnode = tnode.parent
		
		while stack and stack[-1] == 2:
			del stack [-1]
			if stack:
				stack[-1] += 1
			if tnode and tnode.parent:
				isleft = tnode.parent.left == tnode
				tnode = tnode.parent
			dtnode = dtnode.parent
		
		if not stack:
			break
		
		if isleft:
			tnode = tnode.right
			isNucleus = tnode.rightNucleus
		else:
			tnode = tnode.left
			isNucleus = tnode.leftNucleus
		
		while (tnode.leftNucleus or tnode.rightNucleus):
			stack.append(0)
			if tnode.leftNucleus:
				tnode = tnode.left
				isNucleus = tnode.leftNucleus
			else:
				tnode = tnode.right
				isNucleus = tnode.rightNucleus
